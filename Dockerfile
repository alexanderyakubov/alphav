FROM tomcat:8.5

# HOST_NAME is determined from a container like : echo $(/sbin/ip route|awk '/default/ { print $3 }'), but not at BUILD TIME
# 
# https://stackoverflow.com/questions/22944631/how-to-get-the-ip-address-of-the-docker-host-from-inside-a-docker-container
# https://gist.github.com/MauricioMoraes/87d76577babd4e084cba70f63c04b07d
#
# docker run -dit --name alpine-test --network host alpine ash
# docker attach alpine-test
# Inside container: /sbin/ip route|awk '/default/ { print $3 }'
# in my case it gives 192.168.178.1

ENV ALPHAV_POSTGRES_SERVER=127.0.0.1
ENV ALPHAV_POSTGRES_PORT=5432
ENV ALPHAV_POSTGRES_DB_NAME=dbrok
ENV ALPHAV_POSTGRES_USER_NAME=mars
ENV ALPHAV_POSTGRES_USER_PASSWORD=ira
ENV ALPHAV_DAY_RANGE_ONLY=TRUE


EXPOSE 5432

COPY ./target/alphav-1.0.3-RELEASE.war /usr/local/tomcat/webapps/alphav.war


