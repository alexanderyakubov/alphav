
# Alphav data broker

### Recent changes (14.05.2020):

* Callback is realized based on ResultUrl param in src/main/resources/application.properties and can be redefined with a env variable to docker's run command: -e "ALPHAV_RESULT_URL=http://localhost:8080/alphav/autotest"
* Postges: no schema creation script is envisaged, only access params should be specified with env variables. Application will create necessary tables and objects when initiated in docker.
* In case of Postges's schema change it is better to drop all old objects
* If, despite env variables' configuration, java properties file (src/main/resources/application.properties) is still needed to be changed, it is importnant to remember the properties file is put into war class executable. After introduction of changes to the properties file, the app chould be rebuild to compose new war executable.

### ENVIRONMENT VARIABLES (Postgres access):
* export ALPHAV_POSTGRES_SERVER=127.0.0.1
* export ALPHAV_POSTGRES_PORT=5432
* export ALPHAV_POSTGRES_DB_NAME=dbrok
* export ALPHAV_POSTGRES_USER_NAME=mars
* export ALPHAV_POSTGRES_USER_PASSWORD=passw

### ENVIRONMENT VARIABLES (Application config):
* Evironment variable ALPHAV_ACCESS_KEY
com.finmars.dbroker.alphav.accessKey = SOME_KEY

* Evironment variable ALPHAV_RESULT_URL -- call back URL
com.finmars.dbroker.alphav.resultUrl = http://localhost:8080/alphav/autotest

* Evironment variable ALPHAV_NULL_STRING
com.finmars.dbroker.alphav.nullString = NULL

* Evironment variable ALPHAV_DAY_RANGE_ONLY
 If TRUE, broker returns all the data from data source regardless of instrument[i].fields[j].values
com.finmars.dbroker.alphav.dayRangeOnly = FALSE

* Evironment variable ALPHAV_DAY_RANGE_ONLY_GENERATE_DAYS
 If TRUE, broker generates all days under ALPHAV_DAY_RANGE_ONLY=true
com.finmars.dbroker.alphav.dayRangeOnlyGenerateDays = FALSE

* Evironment variable ALPHAV_NO_WEEKLY_RESPONSE
 If TRUE, broker generates all days 
com.finmars.dbroker.alphav.noWeeklyResponse = FALSE



# Getting Started (original java project README.md)

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.2.RELEASE/maven-plugin/)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.2.2.RELEASE/reference/htmlsingle/#boot-features-jpa-and-spring-data)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.2.2.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)

### Guides
The following guides illustrate how to use some features concretely:

* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)

