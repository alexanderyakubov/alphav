#! /bin/bash

mvn package

docker build -t finmars/db_alphav ./

# -p 8881:8080

# Define env variable to specify CALLBACK_URL
# -e "ALPHAV_RESULT_URL=http://localhost:8080/alphav/autotest"
docker run -it --rm -p 8080:8080 --network host \
  -e "ALPHAV_POSTGRES_SERVER=127.0.0.1" \
  -e "ALPHAV_POSTGRES_PORT=5432" \
  -e "ALPHAV_POSTGRES_DB_NAME=dbrok" \
  -e "ALPHAV_POSTGRES_USER_NAME=mars" \
  -e "ALPHAV_POSTGRES_USER_PASSWORD=ira" \
  -e "ALPHAV_DAY_RANGE_ONLY=TRUE" \
  --name finmars_dbrok_alphav finmars/db_alphav 

# TO RUN A TEST: 
# cd test
# ./runtest.sh

