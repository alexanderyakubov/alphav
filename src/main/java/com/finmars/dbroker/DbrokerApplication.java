package com.finmars.dbroker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

// https://www.tutorialspoint.com/spring_boot/spring_boot_tomcat_deployment.htm
// https://docs.spring.io/spring-boot/docs/current/reference/html/howto.html#howto-traditional-deployment

@SpringBootApplication
public class DbrokerApplication  extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	   return application.sources(DbrokerApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(DbrokerApplication.class, args);
	}

}
