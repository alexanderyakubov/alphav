package com.finmars.dbroker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.io.File;


public class DbrokerConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(DbrokerConfig.class.getName());

	//public static String baseUrl = "api.worldtradingdata.com/api/v1/history"; //"http://localhost:7079";
	//public static String accessKey = "xqXGTuEafuFWVnETvbipaPOolcg3GP9elTUGqlyWzYcfW0r6kbZGIdnTOvnR"; //"API-KEY";
	//public static String resultUrl = "http://localhost:8080/autotest";
	
    private static Properties properties; 

    private static String baseURL = "";
    public static String getBaseURL() {return baseURL;}

    private static String resultURL = "";
    public static String getResultURL() {return resultURL;}

    private static String accessKey = "";
    public static String getAccessKey() {return accessKey;}

    private static String nullString = "NULL";
    public static String getNullString() {return nullString;}

    private static boolean noWeeklyResponse = false;
    public static boolean isNoWeeklyResponse() {return noWeeklyResponse;}

    private static boolean dayRangeOnly = false;
    public static boolean isDayRangeOnly() {return dayRangeOnly;}

    private static boolean dayRangeOnlyGenerateDays = false;
    public static boolean isDayRangeOnlyGenerateDays() {return dayRangeOnlyGenerateDays;}


    // https://stackoverflow.com/questions/9259819/how-to-read-values-from-properties-file	
	// https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-external-config
	static {
        properties = new Properties();
        try {
            File file = ResourceUtils.getFile("classpath:application.properties");
            InputStream in = new FileInputStream(file);
            properties.load(in);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }

        baseURL = DbrokerConfig.properties.getProperty("com.finmars.dbroker.alphav.baseUrl").trim();

        String strResultURL = System.getenv("ALPHAV_RESULT_URL");
        resultURL = DbrokerConfig.properties.getProperty("com.finmars.dbroker.alphav.resultUrl").trim();
        if ( strResultURL != null ) {
            strResultURL = strResultURL.trim();
            if ( strResultURL.length()>0 ){
                LOGGER.info("Config redefined resultUrl = " + strResultURL);
                if( resultURL.trim().compareTo( strResultURL ) != 0) {
                    resultURL = strResultURL;
                    LOGGER.info("Config resultURL changed to " + strResultURL);
                }
            }
        }

        String strAccessKey = System.getenv("ALPHAV_ACCESS_KEY");
        accessKey = DbrokerConfig.properties.getProperty("com.finmars.dbroker.alphav.accessKey").trim();
        if ( strAccessKey != null ) {
            strAccessKey = strAccessKey.trim();
            if (strAccessKey.length()>0){
                LOGGER.info("Config redefined accessKey = " + strAccessKey);
                if(accessKey.trim().compareTo( strAccessKey ) != 0) {
                    accessKey = strAccessKey;
                    LOGGER.info("Config accessKey changed to " + strAccessKey);
                }
            }
        }

        String strNullString = System.getenv("ALPHAV_NULL_STRING");
        nullString = DbrokerConfig.properties.getProperty("com.finmars.dbroker.alphav.nullString").trim();
        if ( strNullString != null ) {
            strNullString = strNullString.trim();
            if (strNullString.length()>0){
                LOGGER.info("Config redefined nullString = " + strNullString);
                if(nullString.trim().compareTo(strNullString) != 0) {
                    nullString = strNullString;
                    LOGGER.info("Config nullString changed to " + strNullString);
                }
            }
        }

        String strDayRangeOnly = System.getenv("ALPHAV_DAY_RANGE_ONLY");
        dayRangeOnly = Boolean.parseBoolean( DbrokerConfig.properties.getProperty("com.finmars.dbroker.alphav.dayRangeOnly").trim() );
        if ( strDayRangeOnly != null ) {
            LOGGER.info("Config redefined dayRangeOnly = " + strDayRangeOnly);
            if( Boolean.parseBoolean(strDayRangeOnly) == true) {
                dayRangeOnly = true;
                LOGGER.info("Config dayRangeOnly changed to TRUE");
            }
        }

        String strDayRangeOnlyGenerateDays = System.getenv("ALPHAV_DAY_RANGE_ONLY_GENERATE_DAYS");
        dayRangeOnlyGenerateDays = Boolean.parseBoolean( 
                        DbrokerConfig.properties.getProperty("com.finmars.dbroker.alphav.dayRangeOnlyGenerateDays").trim() 
        );
        if ( strDayRangeOnlyGenerateDays != null ) {
            LOGGER.info("Config redefined dayRangeOnlyGenerateDays = " + strDayRangeOnlyGenerateDays);
            if( Boolean.parseBoolean(strDayRangeOnlyGenerateDays) == true) {
                dayRangeOnlyGenerateDays = true;
                LOGGER.info("Config dayRangeOnlyGenerateDays changed to TRUE");
            }
        }

        String strNoWeeklyResponse = System.getenv("ALPHAV_NO_WEEKLY_RESPONSE");
        noWeeklyResponse = Boolean.parseBoolean( DbrokerConfig.properties.getProperty("com.finmars.dbroker.alphav.noWeeklyResponse").trim() );
        if ( strNoWeeklyResponse != null ) {
            LOGGER.info("Config redefined noWeeklyResponse = " + strNoWeeklyResponse);
            if( Boolean.parseBoolean(strNoWeeklyResponse) == true) {
                noWeeklyResponse = true;
                LOGGER.info("Config noWeeklyResponse changed to TRUE");
            }
        }
    }

}
