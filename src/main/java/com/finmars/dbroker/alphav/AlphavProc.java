package com.finmars.dbroker.alphav;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.hibernate.loader.plan.exec.process.spi.ReturnReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.async.DeferredResult;

import com.finmars.dbroker.DbrokerConfig;
import com.finmars.dbroker.rest.RestField;
import com.finmars.dbroker.rest.RestFieldValue;
import com.finmars.dbroker.rest.RestResponse;


public class AlphavProc implements Runnable {

	// https://www.baeldung.com/spring-boot-logging
	// https://examples.javacodegeeks.com/core-java/lang/system/out/logging-system-println-results-log-file-example/
    final static Logger LOGGER = LoggerFactory.getLogger(AlphavProc.class);
    
    final static int WEEKLY_DAYS_THRESHOLD = 9;

	//@Autowired -- is removed, this is just a link to
	private AlphavStorRepository storRepo;

    public AlphavProc(AlphavStorRepository localStorRepo,
                     DeferredResult<RestResponse> localResult, 
                     RestResponse localResponse)
    {
        this.storRepo = localStorRepo;
        this.result = localResult;
        this.response = localResponse;
    }

    public DeferredResult<RestResponse> result;
    public RestResponse response;


    @Override
    public void run() {
        response.errStatus = 0;
        response.errMsg = "";

        try{
            // Config access > http://dolszewski.com/spring/spring-boot-application-properties-file/

            for( int i=0; i<response.data.items.size(); i++) {

                if (response.data.items.get(i).fields.size() == 0) {
                    RestField restField = new RestField();
                    restField.setCode("close");
                    response.data.items.get(i).fields.add(restField);
                }

                boolean isHttpRequestRequired = false;
                int indFieldStop = 0; 
                int indValueStop = 0;

                if ( DbrokerConfig.isDayRangeOnly() == false ) {
                    for ( int j=0; j<response.data.items.get(i).fields.size(); j++ ) {
                        for ( int v=0; v<response.data.items.get(i).fields.get(j).values.size(); v++ ) {
                            Optional<AlphavStorValue> optStorVal = getStorValue(
                                response.data.items.get(i).reference, 
                                response.data.items.get(i).fields.get(j).getCode(),
                                response.data.items.get(i).fields.get(j).values.get(v).date);
                            if (optStorVal == null) {
                                isHttpRequestRequired = true;
                                indFieldStop = j;
                                indValueStop = v;
                                break;
                            }
                            if (optStorVal.isPresent()) {
                                response.data.items.get(i).fields.get(j).values.get(v).value =
                                         String.format("%f", optStorVal.get().getValue() );
                                response.data.items.get(i).fields.get(j).values.get(v).fromCache = true;
                            } else {
                                response.data.items.get(i).fields.get(j).values.get(v).value =
                                        DbrokerConfig.getNullString();
                                response.data.items.get(i).fields.get(j).values.get(v).fromCache = true;
                            }
                        }
                        if (isHttpRequestRequired) {
                            break;
                        }
                    }    
                } else {
                    // dayRangeOnly == true, no value specifications are listed
                    isHttpRequestRequired = isRequiredHttpRequest_DayRange(i);

                    for ( int j=0; j<response.data.items.get(i).fields.size(); j++ ) {
                        response.data.items.get(i).fields.get(j).values.clear();
                    }
                }

                Boolean wasRequestToProvider = false;
                // We retrieve data starting from startDate from http and put into resulting response
                if ( isHttpRequestRequired ) {

                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

                    AlphavQuery httpQuery = new AlphavQuery();
                    httpQuery.getResult(response.data.items.get(i).reference,
                            addDaysToDate( response.data.getDate_from(), -1 * WEEKLY_DAYS_THRESHOLD ),
                            response.data.getDate_to(), DbrokerConfig.isNoWeeklyResponse()); // false -- both daily and weekly if necessary

                    
                    for ( int j=0; j<response.data.items.get(i).fields.size(); j++ ) {

                        JsonNode historyNode = httpQuery.gRootNode;
                        Iterator<String> strDates = historyNode.fieldNames();
                        while(strDates.hasNext()) {
                            String strDate = strDates.next();
                            Date date;
                            try {
                                date = formatter.parse(strDate);
                            } catch(ParseException e) {
                                throw new RuntimeException("Error in converting date in http response "+e.toString());
                            }
                            JsonNode dateElem = historyNode.get(strDate);
                            String strValue = dateElem.get(
                                    response.data.items.get(i).fields.get(j).getJsonCode() 
                                ).asText();
                            Double value;
                            try {
                                value = Double.valueOf(strValue); 
                            } catch(NumberFormatException e) {
                                throw new RuntimeException("Error in converting value in http response "+e.toString());
                            }

                            
                            AlphavStorValue storVal = new AlphavStorValue( response.data.items.get(i).reference,
                                    // // RestField.FieldCodes.valueOf(response.data.items.get(i).fields.get(j).getCode()),
                                    response.data.items.get(i).fields.get(j).getCode(), value, date, httpQuery.isLastResponseWeekly );
                            saveToStor(storVal);

                            
                            if( date.compareTo(response.data.getDate_from())<0 ) { // date is earlier (smaller) than Date_from
                                break;
                            }
                            
                        }
                    }
                    wasRequestToProvider = true;
                }

                // We retrieve data before startDate from DB and put into resulting response
                if ( DbrokerConfig.isDayRangeOnly() == false ) {
                    for ( int j=indFieldStop; j<response.data.items.get(i).fields.size(); j++ ) {
                        for (   int v = (j==indFieldStop) ? indValueStop : 0 ;
                                v < response.data.items.get(i).fields.get(j).values.size(); 
                                v++ ) 
                        {
                            Optional<AlphavStorValue> optStorVal = getStorValue(
                                response.data.items.get(i).reference, 
                                response.data.items.get(i).fields.get(j).getCode(),
                                response.data.items.get(i).fields.get(j).values.get(v).date
                            );
   
                            if (optStorVal == null || optStorVal.isPresent() == false) {
                                response.data.items.get(i).fields.get(j).values.get(v).value = DbrokerConfig.getNullString();
                                response.data.items.get(i).fields.get(j).values.get(v).fromCache = !wasRequestToProvider;
                                continue;
                            }
                            response.data.items.get(i).fields.get(j).values.get(v).value =
                                    String.format("%f", optStorVal.get().getValue() );
                            response.data.items.get(i).fields.get(j).values.get(v).fromCache = !wasRequestToProvider;
                        }
                        if (isHttpRequestRequired) {
                            break;
                        }
                    }    
                } else {
                    // dayRangeOnly == true, no value specifications are listed

                    for ( int j=0; j<response.data.items.get(i).fields.size(); j++ ) {

                        //data is sorted by DATE ASCENDING
                        List<AlphavStorValue> values;
                        try{
                            values = 
                                storRepo.findByTickerAndDateRange(
                                        response.data.items.get(i).reference, 
                                        response.data.items.get(i).fields.get(j).getCode(),
                                        addDaysToDate(response.data.getDate_from(), -1 * WEEKLY_DAYS_THRESHOLD),
                                        response.data.getDate_to()
                            );
                        } catch (Exception e) {
                            throw new RuntimeException("No data was found at the stage of JSON response generation: "+e.toString());
                        }
                    
                        double lastValue = 0;
                        Date lastDate = new Date();
                        for ( int r = 0; r < values.size(); r++ ) { 
                            AlphavStorValue value = values.get( r );

                            if (response.data.getDate_from().compareTo( value.getDate() ) > 0) { 
                                // value.getDate() is before Date_from
                                lastValue = value.getValue();
                                lastDate = value.getDate();
                                continue;
                            }

                            if (response.data.getDate_from().compareTo( value.getDate() ) == 0) {
                                // lastDate is updated to put the record for Date_from
                                lastDate = addDaysToDate(response.data.getDate_from(), -1); 
                            }

                            // generation of data for missed dates 
                            if (DbrokerConfig.isDayRangeOnlyGenerateDays() == true) {
                                for (   Date iDate = addDaysToDate(lastDate, 1);
                                        iDate.compareTo( value.getDate() ) < 0; 
                                        iDate = addDaysToDate(iDate, 1)
                                )
                                {
                                    if( isWeekEnd(iDate)) {
                                        continue;
                                    }
                                    RestFieldValue resVal = new RestFieldValue();
                                    resVal.value = String.format("%f", lastValue );
                                    resVal.date = iDate;
                                    resVal.fromCache = !wasRequestToProvider;
                                    response.data.items.get(i).fields.get(j).values.add(resVal);
                                }
                            }

                            RestFieldValue resVal = new RestFieldValue();
                            resVal.value = String.format("%f", value.getValue() );
                            resVal.date = value.getDate();
                            resVal.fromCache = !wasRequestToProvider;
                            response.data.items.get(i).fields.get(j).values.add(resVal);

                            lastValue = value.getValue();
                            lastDate = value.getDate();
                        }

                        // generation of data for missed trailing dates 
                        if (DbrokerConfig.isDayRangeOnlyGenerateDays() == true) { 
                            for (   Date iDate = addDaysToDate(lastDate, 1);
                                    iDate.compareTo( response.data.getDate_to() ) < 0; 
                                    iDate = addDaysToDate(iDate, 1)
                            )
                            {
                                if( isWeekEnd(iDate)) {
                                    continue;
                                }
                                RestFieldValue resVal = new RestFieldValue();
                                resVal.value = String.format("%f", lastValue );
                                resVal.date = iDate;
                                resVal.fromCache = !wasRequestToProvider;
                                response.data.items.get(i).fields.get(j).values.add(resVal);
                            }
                        }
                    }

                } // else
            }

            this.postResult();

        } catch (Exception e) {
            //logger.error(e.getMessage());
            response.errStatus = 1;
            response.errMsg = e.getMessage();
            result.setErrorResult(response);
            return;
        } 

        result.setResult(response);
    }

    // returns:
    //      null if empty and NOT weekend
    //      optional.empty if empty and weekend
    public Optional<AlphavStorValue> getStorValue(String pInstrument, String pField, Date pDate) {
        Optional<AlphavStorValue> optStorVal =
             this.storRepo.findById(new AlphavStorValueIndex(pInstrument, pField, pDate));
        if ( ! optStorVal.isPresent() ) {
            // if the current day is on weekend, return optional.empty()
            if ( isWeekEnd(pDate) == true ) {
                return Optional.empty();
            } else {
                return null;
            }
        }
        return optStorVal;
    }

    public boolean isWeekEnd(Date pDate){
        Calendar c = Calendar.getInstance();
        c.setTime(pDate);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        // if the current day is on weekend, return optional.empty()
        if ( dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY ) {
            return true;
        }
        return false;
    }


    public boolean isRequiredHttpRequest_DayRange( int indInstrument ) {

        for ( int j=0; j<response.data.items.get( indInstrument ).fields.size(); j++ ) {

            Optional<AlphavStorValue> optMinDateStorVal = getMinDateStorValue(
                    response.data.items.get( indInstrument ).reference, 
                    response.data.items.get( indInstrument ).fields.get(j).getCode(),
                    addDaysToDate( response.data.getDate_from(), -1 * WEEKLY_DAYS_THRESHOLD )
            );
            if (optMinDateStorVal.isPresent() == false) {
                return true;
            }
            if (optMinDateStorVal.get().getDate().compareTo( response.data.getDate_from() ) > 0) { 
                // min_date is later date_from
                return true;
            }    
    

            Date maxDate = storRepo.getMaxDateByTicker(
                    response.data.items.get( indInstrument ).reference, 
                    response.data.items.get( indInstrument ).fields.get(j).getCode(),
                    response.data.getDate_to()
            );

            if (maxDate != null ) {
                if (maxDate.compareTo( response.data.getDate_to() ) < 0) {
                    return true;
                }
            }

        }
        return false;
    }


    // weekly data cannot overwrite daily date, but daily data must overwrite weekly
    // returns true if saved 
    public boolean saveToStor( AlphavStorValue newVal ) {
        Optional<AlphavStorValue> optStorVal = 
             this.storRepo.findById(new AlphavStorValueIndex(newVal.getInstrument(), newVal.getField(), newVal.getDate()));
        if (optStorVal.isPresent()) {
            if ( optStorVal.get().getIsWeekly()==false ) {
                return false;
            } else { // storVal is weekly
                if ( newVal.getIsWeekly()==true ) {
                    return false;
                }                
            }
        }
        this.storRepo.save( newVal );
        return true;
    }


    public Optional<AlphavStorValue> getMinDateStorValue(String pInstrument, String pField, Date pDateFrom) {
        Date minDate = storRepo.getMinDateByTicker(pInstrument, pField, pDateFrom);
        if (minDate == null) {
            return Optional.empty();
        }
        return storRepo.findById(new AlphavStorValueIndex(pInstrument, pField, minDate));
    }


    public Date getStartDate(Date startDate, List<AlphavStorValue> values) {
        Date prevDate = response.data.getDate_from();
        for ( AlphavStorValue value : values ) {
            int days = getDifferenceDays(prevDate, value.getDate());
            if (days>3) {
                if (getDifferenceDays(prevDate, startDate)>0) {
                    return prevDate;
                }
            }

            Calendar c = Calendar.getInstance();
            c.setTime(value.getDate());
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            // if the current day is Monday then 2 day break is OK
            if ( days>1 &&  dayOfWeek!=Calendar.MONDAY && dayOfWeek!=Calendar.SUNDAY ) {
                if (getDifferenceDays(prevDate, startDate)>0) {
                    return prevDate;
                }
            }
            prevDate = value.getDate();
        }
        if (getDifferenceDays(prevDate, startDate)>0) {
            return prevDate;
        }
        return startDate;
    }

    // days between d2 and d1. Positive if d2 is after d1.
    public int getDifferenceDays(Date d1, Date d2) {
        int daysdiff = 0;
        long diff = d2.getTime() - d1.getTime();
        long diffDays = Math.round(diff / (24 * 60 * 60 * 1000)); //+ 1;
        daysdiff = (int) diffDays;
        return daysdiff;
    }

    public Date addDaysToDate(Date date, int days){
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DAY_OF_MONTH, days); 
        return c.getTime();
    } 

    private void postResult() throws Exception {
        String resultUrl = DbrokerConfig.getResultURL();
        if( resultUrl==null ){
            throw new RuntimeException("No com.finmars.dbroker.alphav.resultUrl property is found");
        }

        ObjectMapper mapper = new ObjectMapper();
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(resultUrl);
     
        String json = mapper.writeValueAsString(response);

        StringEntity entity = new StringEntity(json);
        httpPost.setEntity(entity);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
     
        CloseableHttpResponse response = client.execute(httpPost);
        if (response.getStatusLine().getStatusCode() != 200) {
            client.close();
            throw new RuntimeException("Executing 'Post of the Result': code not 200, The code=" +
                 Integer.toString(response.getStatusLine().getStatusCode()) + " " + response.getStatusLine().getReasonPhrase() );
        }
        client.close();
    }

}
