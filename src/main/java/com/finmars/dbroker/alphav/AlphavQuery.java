package com.finmars.dbroker.alphav;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import javax.management.RuntimeErrorException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finmars.dbroker.DbrokerConfig;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.CharEncoding;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

// https://mkyong.com/java/apache-httpclient-examples/
// https://hc.apache.org/httpcomponents-asyncclient-4.1.x/examples.html -- async

// https://www.baeldung.com/jackson-object-mapper-tutorial
// https://stackoverflow.com/questions/2525042/how-to-convert-a-json-string-to-a-mapstring-string-with-jackson-json

// https://howtodoinjava.com/spring-boot2/resttemplate/resttemplate-post-json-example/ -- Spring RestTemplate
// https://stackoverflow.com/questions/7172784/how-do-i-post-json-data-with-curl -- cURL header:"application/json"
public class AlphavQuery {

    public JsonNode gRootNode;
    public boolean isLastResponseWeekly;

    public void getResult(String ticker, Date fromDate, Date toDate, boolean isOnlyDaily) throws RuntimeException {
        isLastResponseWeekly = false;

        String baseUrl = DbrokerConfig.getBaseURL();
        if( baseUrl == null ) {
            throw new RuntimeException("No com.finmars.dbroker.alphav.baseUrl property is found");
        }
        String accessKey = DbrokerConfig.getAccessKey();
        if( accessKey == null ) {
            throw new RuntimeException("No com.finmars.dbroker.alphav.accessKey property is found");
        }

        //SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        //String strDateFrom = formatter.format(fromDate); // response.data.getDate_from()
        //String strDateTo = formatter.format(toDate); // response.data.getDate_to()

        // https://stackoverflow.com/questions/29858248/reading-value-of-nested-key-in-json-with-java-jackson
        String strURL = baseUrl + "?function=TIME_SERIES_DAILY";
        //strURL = strURL + "&date_from=" + strDateFrom + "&date_to=" + strDateTo;
        strURL = strURL +  "&symbol=" + ticker; 
        strURL = strURL + "&apikey=" + accessKey;
        String res = getUrl( strURL );
        parse( res, "Time Series (Daily)" );
        
        if ( getEarliestDate(fromDate) == null ){
            return;
        }

        strURL = baseUrl + "?function=TIME_SERIES_DAILY_ADJUSTED&outputsize=full";
        //strURL = strURL + "&date_from=" + strDateFrom + "&date_to=" + strDateTo;
        strURL = strURL +  "&symbol=" + ticker;
        strURL = strURL + "&apikey=" + accessKey;
        res = getUrl( strURL );
        parse( res, "Time Series (Daily)" );

        Date earliestDate = getEarliestDate(fromDate);
        if ( earliestDate == null ){
            return;
        }
        if( isOnlyDaily ) {
            throw new RuntimeException("No DAILY history data for this date ("+ fromDate.toString()
                    +") -- the date seems to be too early and deep in times."
                    +" The earliest date of the response: " + earliestDate.toString());
        }
    
        isLastResponseWeekly = true;

        strURL = baseUrl + "?function=TIME_SERIES_WEEKLY_ADJUSTED&outputsize=full";
        //strURL = strURL + "&date_from=" + strDateFrom + "&date_to=" + strDateTo;
        strURL = strURL +  "&symbol=" + ticker;
        strURL = strURL + "&apikey=" + accessKey;
        res = getUrl( strURL );
        parse( res, "Weekly Adjusted Time Series" );

        earliestDate = getEarliestDate(fromDate);
        if ( earliestDate == null ){
            return;
        } else {
            throw new RuntimeException("No WEEKLY history data for this date ("+ fromDate.toString()
                    +") -- the date seems to be too early and deep in times."
                    +" The earliest date of the response: " + earliestDate.toString());
        }

    }

    // returns NULL if the ealiest date is ealier than fromDate
    //    else returns the ealiest date wich is later then fromDate 
    private Date getEarliestDate( Date fromDate ) {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        //JsonNode historyNode = this.gRootNode.path("Time Series (Daily)");
        JsonNode historyNode = this.gRootNode;
        Iterator<String> strDates = historyNode.fieldNames();
        Date earliestDate = new Date();

        while(strDates.hasNext()) {
            String strDate = strDates.next();

            Date date;
            try{
                date = formatter.parse(strDate);
            } catch (ParseException pe) {
                throw new RuntimeException("Error in time series parsing: date '"+strDate+"' not in format yyyy-MM-dd");
            }

            if (date.compareTo(fromDate) < 0) {
                return null;
            }

            if (date.compareTo(earliestDate) < 0) {
                earliestDate = date;
            }
        }
        return earliestDate;
    } 
    
    // @param uri: does not include prfix "https://"
    private String getUrl(String uri) {
        HttpGet request;
        try {
            request = new HttpGet("https://"+uri);
        } catch(IllegalArgumentException agrExc) {
            throw new RuntimeException("Incorrect URL: "+uri);
        }
        // add request headers
        //request.addHeader("custom-key", "mkyong");
        //request.addHeader(HttpHeaders.USER_AGENT, "Googlebot");

        //  STREAM : https://stackoverflow.com/questions/49346948/how-to-get-httpclient-response-as-stream
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
            CloseableHttpResponse response = httpClient.execute(request)) {

            if(response.getStatusLine().getStatusCode()!=200){
                Integer errCode = response.getStatusLine().getStatusCode();
                String errMsg = response.getStatusLine().getReasonPhrase();
                System.out.println( "Response code: " + errCode.toString() );
                System.out.println( errMsg );
                throw new RuntimeException("Return from http reguest to data provider: " + errCode.toString() + "." + errMsg);
            }

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                // return it as a String
                //return EntityUtils.toString(entity);
                try (InputStream inputStream = entity.getContent()) {
                    String text = IOUtils.toString(inputStream, CharEncoding.UTF_8);
                    return text;
                }
            }

        } catch (Exception e) {
            throw new RuntimeException("ERROR in http reguest: " + e.toString());
        }
        return "";
    }

    // https://www.journaldev.com/2324/jackson-json-java-parser-api-example-tutorial
    private void parse(String json, String historyNodeName) {
        ObjectMapper mapper = new ObjectMapper(); 

        /*
        TypeReference<HashMap<String,Object>> typeRef 
                = new TypeReference<HashMap<String,Object>>() {};    
        parsedJson = mapper.readValue(json, typeRef);
        */

        //gParsedJson = mapper.readValue(json, gParsedJson.getClass());
        try{
            this.gRootNode = mapper.readTree(json);
        } catch (Exception e) {
            throw new RuntimeException("ERROR: Return from http reguest is not a JSON: " + e.toString());
        }
        this.gRootNode = this.gRootNode.path( historyNodeName );
    }
    
    public String test(String[] tickers, String strDate) {

		Date histDate;
		
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			histDate = formatter.parse(strDate);			
		} catch(Exception e) {
			return e.getMessage();
		}

		try{
			// https://stackoverflow.com/questions/29858248/reading-value-of-nested-key-in-json-with-java-jackson
			// https://www.programcreek.com/java-api-examples/?class=com.fasterxml.jackson.databind.JsonNode&method=fieldNames
			AlphavQuery alphav = new AlphavQuery();
			String res = alphav.getUrl("www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=MSFT&apikey=391XB5NR3B623XT8");
			//alphav.parse(res, "Time Series (Daily)" );
            ObjectMapper mapper = new ObjectMapper(); 
            this.gRootNode = mapper.readTree(res);
			
			JsonNode metaNode = alphav.gRootNode.path("Meta Data");
			String strOutput = "Name: " + metaNode.path("2. Symbol").asText();

			JsonNode historyNode = alphav.gRootNode.path("Time Series (Daily)");
			//Iterator<JsonNode> elements = historyNode.elements();
			Iterator<String> elements = historyNode.fieldNames();
			while(elements.hasNext()){
				String fieldName = elements.next();
				JsonNode dateElem = historyNode.get(fieldName);
				//System.out.println("Phone No = "+phone.asLong());
				strOutput = strOutput +", Date "+fieldName+": "+ dateElem.toString() ;
			}
			return  strOutput; 
		} catch (Exception e) {
			//logger.error(e.getMessage());
			return e.getMessage();
		}

    }
}
