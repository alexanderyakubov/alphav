package com.finmars.dbroker.alphav;


import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AlphavStorRepository extends JpaRepository<AlphavStorValue, AlphavStorValueIndex> {

    List<AlphavStorValue> findByInstrument(String instrument);

    Optional<AlphavStorValue> findById(AlphavStorValueIndex id);

    @Query(
    //  "SELECT val FROM AlphavStorValue val AMD AlphavStorInstrument inst WHERE inst.id=val.instrument_id AND " +
    //     " inst.ticker=(:pTicker) AND val.field=(:pField) AND  val.date >= (:pDateFrom)  AND " +
    //     " val.date <= (:pDateTo) ORDER BY val.date"
      "SELECT val FROM AlphavStorValue val WHERE " +
          " val.instrument=(:pInstrument) AND val.field=(:pField) AND val.date >= (:pDateFrom)  AND " +
          " val.date <= (:pDateTo) ORDER BY val.date"
    )
    List<AlphavStorValue> findByTickerAndDateRange(
            @Param("pInstrument") String pInstrument,
            @Param("pField") String pField, 
            @Param("pDateFrom") Date pDateFrom,
            @Param("pDateTo") Date pDateTo
    );

    @Query(
        "SELECT MIN(val.date) FROM AlphavStorValue val WHERE " +
           " val.instrument=(:pInstrument) AND val.field=(:pField) AND val.date >= (:pDateFrom)"
    )
    Date getMinDateByTicker(
            @Param("pInstrument") String pInstrument,
            @Param("pField") String pField,
            @Param("pDateFrom") Date pDateFrom
    );

    @Query(
        "SELECT MIN(val.date) FROM AlphavStorValue val WHERE " +
           " val.instrument=(:pInstrument) AND val.field=(:pField) AND val.date >= (:pDateFrom) AND" +
           " val.isWeekly=false"
    )
    Date getMinDateDailyByTicker(
            @Param("pInstrument") String pInstrument,
            @Param("pField") String pField,
            @Param("pDateFrom") Date pDateFrom
    );

    @Query(
        "SELECT MAX(val.date) FROM AlphavStorValue val WHERE " +
           " val.instrument=(:pInstrument) AND val.field=(:pField) AND val.date <= (:pDateTo)"
    )
    Date getMaxDateByTicker(
            @Param("pInstrument") String pInstrument,
            @Param("pField") String pField,
            @Param("pDateTo") Date pDateTo
    );

}


//public interface AlphavStorRepository {}
