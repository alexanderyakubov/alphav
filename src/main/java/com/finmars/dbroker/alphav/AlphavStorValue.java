package com.finmars.dbroker.alphav;

import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Index;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.validation.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finmars.dbroker.rest.RestField;

import org.apache.commons.lang3.time.DateUtils;

// JPA composit indices: https://medium.com/code-smells/composite-keys-in-jpa-e6f4e35b3287
@Configuration
@Table(name=AlphavStorValue.TABLE_NAME)
@Entity
@IdClass(AlphavStorValueIndex.class)
public class AlphavStorValue {
    //	https://spring.io/guides/gs/accessing-data-jpa/
    // one_to_many	
    // https://examples.javacodegeeks.com/enterprise-java/spring/data/spring-data-jparepository-example/

    public final static String TABLE_NAME = "alphav_stor_value";
    public final static String INSTRUMENT = "instrument";
    public final static String FIELD = "field";
    public final static String DATE = "date";

    @Autowired
    public void configureJackson(ObjectMapper objectMapper) {
        objectMapper.setTimeZone(TimeZone.getDefault());
    }


    //@Id
    //@GeneratedValue(strategy=GenerationType.AUTO)
    //@JsonIgnore
    //private Long id;

    //@ManyToOne
    //@JoinColumn(name="instrument_id")
    //private AlphavStorInstrument instrument;
    @Id
    @Column(name=INSTRUMENT)
    private String instrument;

    //@Size(min=3,max=3)
    //@Enumerated(EnumType.STRING)
    //private RestField.FieldCodes field; // "open", "close", etc
    @Id
    @Column(name=FIELD)
    private String field; // "open", "close", etc

    // https://stackoverflow.com/questions/29076210/convert-a-object-into-json-in-rest-service-by-spring-mvc
    // https://www.baeldung.com/spring-boot-formatting-json-dates
    //@DateTimeFormat(pattern="yyyy-MM-dd")
    // https://stackoverflow.com/questions/46011703/set-current-timezone-to-jsonformat-timezone-value
    @Id
    @Column(name=DATE)
    @JsonFormat(pattern="yyyy-MM-dd", timezone = JsonFormat.DEFAULT_TIMEZONE)
    private Date date;

    @Column
    private Double value;

    @Column
    private Boolean isWeekly;

    protected AlphavStorValue() {}

    //public AlphavStorValue(AlphavStorInstrument instrument, RestField.FieldCodes field, Double value, Date date) {
    public AlphavStorValue(String instrument, String field, Double value, Date date, boolean isWeekly) {
        this.instrument = instrument;
        this.field = field;
        this.value = value;
        this.date = date;
        this.isWeekly = isWeekly;
    }

    @Override
    public String toString() {
    return String.format(
        "date=%s, instrument='%s', field='%s', value='%f']",
        date.toString(), instrument, field, value);
    }

    //public Long getId() {
    //    return id;
    //}

    // @return AlphavStorInstrument return the instrument
    // public AlphavStorInstrument getInstrument() {
    public String getInstrument() {
        return instrument;
    }

    // @param instrument the instrument to set
    //public void setInstrument(AlphavStorInstrument instrument) {
    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    // @return Double return the value
    public Double getValue() {
        return value;
    }

    // @param value the value to set
    public void setValue(Double value) {
        this.value = value;
    }

    // @return Date return the date
    public Date getDate() {
        return date;
    }

    // @param date the date to set
    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getIsWeekly() {
        return isWeekly;
    }

    public void setIsWeekly(Boolean isWeekly) {
        this.isWeekly = isWeekly;
    }
}

