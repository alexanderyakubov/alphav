package com.finmars.dbroker.alphav;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

// https://medium.com/code-smells/composite-keys-in-jpa-e6f4e35b3287
public class AlphavStorValueIndex implements Serializable {
 
    private String instrument;

    private String field; // "open", "close", etc

    @JsonFormat(pattern="yyyy-MM-dd", timezone = JsonFormat.DEFAULT_TIMEZONE)
    private Date date;

    public String getInstrument() {
        return instrument;
    }

    // @param instrument the instrument to set
    //public void setInstrument(AlphavStorInstrument instrument) {
    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    // @return Date return the date
    public Date getDate() {
        return date;
    }

    // @param date the date to set
    public void setDate(Date date) {
        this.date = date;
    }

    public AlphavStorValueIndex(String instrument, String field, Date date) {
        this.instrument = instrument;
        this.field = field;
        this.date = date;
    }

    public AlphavStorValueIndex() {
    }


}