package com.finmars.dbroker.rest;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;



public class RestData {
    @Autowired
    public void configureJackson(ObjectMapper objectMapper) {
        objectMapper.setTimeZone(TimeZone.getDefault());
    }

    @JsonFormat(pattern="yyyy-MM-dd", timezone = JsonFormat.DEFAULT_TIMEZONE)
	private Date date_from;

	@JsonFormat(pattern="yyyy-MM-dd", timezone = JsonFormat.DEFAULT_TIMEZONE)
	private Date date_to;

	public List<RestInstr> items;


	public RestData() {
		this.items = new ArrayList<RestInstr>();
	}

	public void setDate_from(Date date) {
		this.date_from = DateUtils.truncate(date, Calendar.DATE);
	}

	public Date getDate_from() {
		return this.date_from;
	}

	public void setDate_to(Date date) {
		this.date_to = DateUtils.truncate(date, Calendar.DATE);
	}

	public Date getDate_to() {
		return this.date_to;
	}

}