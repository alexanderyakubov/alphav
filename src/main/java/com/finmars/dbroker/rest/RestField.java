package com.finmars.dbroker.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;


public class RestField {

    @Autowired
    public void configureJackson(ObjectMapper objectMapper) {
        objectMapper.setTimeZone(TimeZone.getDefault());
    }

	private String code;
	public List<String> parameters;
	public List<RestFieldValue> values;

	public RestField() {
		//this.code = "";
		this.parameters = new ArrayList<String>();
		this.values = new ArrayList<RestFieldValue>();
	}

	//@JsonIgnore
	private enum FieldCodes {open, high, low, close, volume};
	//@JsonIgnore
	private static Map<Integer, String> JsonCodes;
	static {
		JsonCodes = new HashMap<>();
		JsonCodes.put(FieldCodes.valueOf("open").ordinal(),"1. open");
		JsonCodes.put(FieldCodes.valueOf("high").ordinal(),"2. high");
		JsonCodes.put(FieldCodes.valueOf("low").ordinal(),"3. low");
		JsonCodes.put(FieldCodes.valueOf("close").ordinal(),"4. close");
		JsonCodes.put(FieldCodes.valueOf("volume").ordinal(),"5. volume");
	};

	public void setCode(String newCode) {
		this.code = FieldCodes.valueOf(newCode).name();
	}

	public String getCode() {
		return this.code;
	}

	public String getJsonCode() {
		return JsonCodes.get(FieldCodes.valueOf(this.code).ordinal());
	}

}