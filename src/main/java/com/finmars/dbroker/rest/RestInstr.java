package com.finmars.dbroker.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;



public class RestInstr {
    @Autowired
    public void configureJackson(ObjectMapper objectMapper) {
        objectMapper.setTimeZone(TimeZone.getDefault());
    }

    public String reference; // ticker or isin of the instrument
    public List<String> parameters;
	public List<RestField> fields;

	public RestInstr() {
		this.parameters = new ArrayList<String>();
		this.fields = new ArrayList<RestField>();
	}

}
