package com.finmars.dbroker.rest;

import java.util.TimeZone;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;


public class RestResponse {
    @Autowired
    public void configureJackson(ObjectMapper objectMapper) {
        objectMapper.setTimeZone(TimeZone.getDefault());
    }

	
	public Integer errStatus;
	public String errMsg;

	public Integer procedure;
	public Object user;
	public RestData data;

}