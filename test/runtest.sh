#! /bin/bash

#http://localhost:8881/wtrad/ -- old URL

# https://finmars.com/services/brokers/wtrade/wtrad/history

# this env variables should be defined locally or in docker container:
# export ALPHAV_POSTGRES_SERVER=localhost
# export ALPHAV_POSTGRES_PORT=5432
# export ALPHAV_POSTGRES_DB_NAME=dbrok
# export ALPHAV_POSTGRES_USER_NAME=mars
# export ALPHAV_POSTGRES_USER_PASSWORD=ira
# If TRUE, broker returns all the data from data source regardless of instrument[i].fields[j].values
# export ALPHAV_DAY_RANGE_ONLY=TRUE

# export ALPHAV_RESULT_URL=http://localhost:8080/alphav/autotest

curl -i -X POST \
 --data "@req_full_1.json" --header "Content-Type: application/json" http://localhost:8080/alphav/prices


